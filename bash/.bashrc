# .bashrc

# shell interactiveness
[[ $- != *i* ]] && return


# settings
bind "set completion-ignore-case on"
bind -x '"\C-l":clear'
shopt -s extglob
shopt -s globstar


# environment variables
export EDITOR=nvim
export FZF_DEFAULT_OPTS="--scheme=path --tiebreak=pathname,length --reverse --preview-window='down,80%' --style=minimal --info=inline-right --color='hl:#$HEX_RED,bg+:#$HEX_BLACK,gutter:#$HEX_BLACK,hl+:#$HEX_RED,border:#$HEX_GREY,info:#$HEX_YELLOW,prompt:#$HEX_GREEN,pointer:#$HEX_HIGHLIGHT'"
export PASSWORD_STORE_DIR=/home/eray/documents/.credentials/passwords

TERM=xterm-256color

HISTCONTROL=ignorespace:ignoredups:erasedups
HISTFILESIZE=2000
HISTSIZE=1000

if [ $USER == "eray" ]; then
    PATH=$PATH:/home/eray/informatics/os/linux/scripts
fi


# colors
BLACK=0
BLUE=4
CYAN=6
GREEN=2
MAGENTA=5
RED=1
WHITE=7
YELLOW=3


# prompt
PROMPT_BLACK="$(tput setaf $BLACK)"
PROMPT_BLUE="$(tput setaf $BLUE)"
PROMPT_CYAN="$(tput setaf $CYAN)"
PROMPT_GREEN="$(tput setaf $GREEN)"
PROMPT_MAGENTA="$(tput setaf $MAGENTA)"
PROMPT_RED="$(tput setaf $RED)"
PROMPT_WHITE="$(tput setaf $WHITE)"
PROMPT_YELLOW="$(tput setaf $YELLOW)"

PROMPT_BOLD="$(tput bold)"
PROMPT_ITALIC="$(tput sitm)"
PROMPT_RESET="$(tput sgr0)"

PS1="\[${PROMPT_BOLD}${PROMPT_GREEN}\]>\[${PROMPT_RESET}\] "


# eza
EZA_BOLD=1
EZA_ITALIC=3
EZA_UNDERLINE=4

eza="ur=3$MAGENTA:uw=3$BLUE:ux=3$CYAN:ue=3$CYAN:gr=3$MAGENTA:gw=3$BLUE:gx=3$CYAN:tr=3$MAGENTA:tw=3$BLUE:tx=3$CYAN"
eza="$eza:sn=3$GREEN:sb=$EZA_BOLD;3$GREEN"
eza="$eza:uu=3$CYAN:gu=3$CYAN:uR=$EZA_ITALIC;3$RED:gR=$EZA_ITALIC;3$RED"
eza="$eza:da=$EZA_ITALIC;3$WHITE"
eza="$eza:ga=3$GREEN:gm=3$YELLOW:gd=$RED:gv=$YELLOW:gt=$YELLOW"
eza="$eza:di=$EZA_BOLD;3$BLUE:ex=$EZA_ITALIC;3$GREEN:ln=$EZA_ITALIC;3$CYAN"
eza="$eza:lp=$EZA_BOLD;3$BLUE"
eza="$eza:*.jpeg=3$MAGENTA:*.jpg=3$MAGENTA:*.png=3$MAGENTA:*.svg=3$MAGENTA:*.mp3=3$CYAN:*.mp4=3$MAGENTA:*.avi=3$MAGENTA:*.mpg=3$MAGENTA"
eza="$eza:*.pdf=3$WHITE:*.docx=3$WHITE:*.pptx=3$WHITE:*.xlsx=3$WHITE:*.md=3$WHITE"
eza="$eza:*.cs=3$BLUE:*.css=3$CYAN:*.html=3$GREEN:*.java=3$YELLOW:*.js=3$YELLOW:*.lua=3$CYAN:*.py=3$GREEN:*.sql=3$RED:*.ts=3$YELLOW"
eza="$eza:*.gpg=38;5;245:*rsa=38;5;245:*rsa.pub=38;5;245"

export EZA_COLORS=$eza


# aliases
alias cat="__cat"
alias cd="__cd"
alias cp="cp -r"
alias grep="grep --color=auto"
alias ip="ip -color=always"
alias la="clear && eza -lga --time-style=long-iso --git --group-directories-first"
alias ls="clear && eza -la --no-permissions --no-filesize --no-user --no-time --git --group-directories-first"
alias mkdir="mkdir -p"
alias pass="GNUPGHOME=/home/eray/documents/.credentials/gpg-key pass"
alias timeshift-gtk="sudo -E setsid -f timeshift-gtk"
alias zip="zip -r"

alias add="git add"
alias branch="git branch -a"
alias checkout="git checkout"
alias clone="git clone"
alias commit="git commit"
alias fetch="git_fetch"
alias pull="git pull"
alias push="git push"
alias rebase="git rebase"
alias stash="git stash"
alias status="git status"

alias ..="cd .."
alias :q="exit"
alias configuration_files="cd /home/eray/informatics/os/linux/configuration_files"
alias d="source fzf.sh d"
alias decrypt="GNUPGHOME=/home/eray/documents/.credentials/gpg-key gpg -d"
alias encrypt="GNUPGHOME=/home/eray/documents/.credentials/gpg-key gpg -r A_ER_Y -se"
alias f="fzf.sh"
alias nowrap="tput rmam"
alias print="lp -d brother_mfc_l2700dw -o print-quality=5 -o media=letter -o sides=two-sided-long-edge"
alias projects="cd /home/eray/informatics/work/jobs/inuits/projects"
alias scan="scanimage --device 'airscan:w1:Brother MFC-L2700DW series' --progress --mode color --format=png --output-file"
alias scripts="cd /home/eray/informatics/os/linux/scripts"
alias su_preserved="sudo -E bash --rcfile ~eray/.bashrc"
alias tree="clear && eza -Ta -I .git --git-ignore --group-directories-first"
alias vim="nvim"
alias wrap="tput smam"


# functions
__cat() {
    \cat $@ | tee >(wl-copy -n)
}

__cd() {
    [[ "$1" == "-" ]] && \cd - > /dev/null

    [[ "$1" == "" ]] && \cd ||
    [[ -a "$1" ]] && \cd "$1" ||
    \cd *"$1"* 2> /dev/null ||
    \cd *"$1" 2> /dev/null

    ls
}

cdr() {
    readarray -d"/" -t directories <<< $(pwd | tr "[:upper:]" "[:lower:]")

    directory_path=""
    for (( i = 2; i < ${#directories[*]}; i++ )); do
        directory_path="$directory_path../"
        [[ $(echo ${directories[-$i]}) =~ .*$1.* ]] && break
    done

    [[ $i -ne ${#directories[*]} ]] && cd $directory_path ||
    (echo "no such directory in pwd" && pwd)
}

clear_docker() {
    docker container rm -fv $(docker container ls -aq) 2> /dev/null
    docker container prune -f
    docker volume rm -f $(docker volume ls -q) 2> /dev/null
    docker volume prune -f
    docker network prune -f
    docker builder prune -af
    docker image rm -f $(docker images | tr -s " " | sed "s/ /:/" | grep -ivE "$(grep -E "^[^#]" /home/eray/informatics/os/linux/configuration_files/docker_images.txt)" | cut -d" " -f2 | xargs) 2> /dev/null
}

git_fetch() {
    if [ "$1" != "" ]; then
        git checkout $1

        if [ $? -eq 0 ]; then
            git fetch --all --prune
            git for-each-ref --format '%(refname:short) %(upstream:track)' refs/heads | awk -v BRANCH=$1 '$1 != BRANCH {print $1}' | xargs -r git branch -D

            echo && git clean -fxn . --exclude=".env*" --exclude=".npm*" --exclude="docker" --exclude="inuits-apollo-server-auth" --exclude="queries.ts" --exclude="requirements*" --exclude="session-vue-3-oidc-library" --exclude="type-defs.ts"
            read -p "Do you really want to delete those files/folders? [y/N] " REMOVE
            if [ "$REMOVE" == "y" ]; then
                git clean -fx . --exclude=".env*" --exclude=".npm*" --exclude="docker" --exclude="inuits-apollo-server-auth" --exclude="queries.ts" --exclude="requirements*" --exclude="session-vue-3-oidc-library" --exclude="type-defs.ts"
            fi
        fi
    else
        echo "branch name expected as argument"
    fi
}

help() {
    cd /home/eray/informatics/archive/$1
}

http() {
    /usr/bin/http --auth-type=jwt --auth="$TOKEN" --verify=no $@
}

http-proxy() {
    PORT=$(grep $1 /home/eray/informatics/work/jobs/inuits/port-proxy_mapping.txt | cut -d" " -f1)
    shift
    /usr/bin/http --proxy=https:socks5h://localhost:$PORT --auth-type=jwt --auth="$TOKEN" --verify=no $@
}

mcat() {
    \cat $@ | tr -d "\n " | tee >(wl-copy -n) && echo
}

new() {
    [ ! -e $1 ] && (touch $1 && echo "# $1" > $1 && nvim $1) || open $1
}

open() {
    FILE_TYPE=$(file -- $1 | grep -v text | grep -v empty | grep -vi ifp)
    if [ "$FILE_TYPE" == "" ]; then
        nvim $1
    else
        case ${!#} in
            *.pdf)                              setsid -f zathura -P 1 "$@" ;;
            *.jpeg | *.jpg | *.png | *.svg)     setsid -f swayimg "$@"      ;;
            *.mp3 | *.mp4 | *.avi | *.mpg)      mpv "$@"                    ;;
            https://youtu*)                     setsid -f freetube "$@"     ;;
            http*)                              setsid -f qutebrowser "$@"  ;;
            *)                                  echo "cannot open '$1'"     ;;
        esac
    fi
}

screenrecord() {
    pactl load-module module-null-sink sink_name=combined
    pactl load-module module-loopback sink=combined source=$(pactl get-default-source)
    pactl load-module module-loopback sink=combined source=$(pactl get-default-sink).monitor

    wf-recorder --audio=combined.monitor --codec h264_vaapi --framerate 60 --no-damage -f /home/eray/temporary/screen_recording.mp4

    pactl unload-module module-loopback
    pactl unload-module module-null-sink
}

search() {
    grep -iEn "$1" ** 2> /dev/null
}

template() {
    if [ "$2" != "" ]; then
        CLASS_NAME=$(cut -d'.' -f1 <<< $2)
        PACKAGE_NAME=$(pwd | sed "s/\//\./g")

        \cat /home/eray/informatics/archive/$1/template.txt | sed "s/CLASS_NAME/$CLASS_NAME/g" | sed "s/PACKAGE_NAME/$PACKAGE_NAME/g"> $2 && nvim $2
    elif [ "$1" != "" ]; then
        \cat /home/eray/informatics/archive/$1/template.txt
    else
        echo "cannot load template"
    fi
}

venv() {
    directory_path="./"

    if [ "$1" != "install" ]; then
        if [[ $(grep -o "/client-collection-module" <<< $PWD) != "" ]]; then
            directory_path="/home/eray/informatics/work/jobs/inuits/projects/elody/repositories/elody-common/collection-api"
        else
            readarray -d"/" -t directories <<< $(pwd | tr "[:upper:]" "[:lower:]")

            for (( i = 1; i < ${#directories[*]}; i++ )); do
                directory_content=$(\ls -A $directory_path)
                [[ $(echo $directory_content | grep -o ".venv") == ".venv" ]] && break
                directory_path="$directory_path../"
            done
        fi
    else
        python -m venv .venv
    fi

    if [[ $(grep -o "/client-collection-module" <<< $PWD) != "" || $i -ne ${#directories[*]} || "$1" == "install" ]]; then
        source ${directory_path}/.venv/bin/activate && ls

        [[ $? -eq 0 && "$1" == "install" ]] &&
        pip install --no-cache-dir $(\cat requirements*.txt)
    fi
}

export -f open


# auto run
[[ $(grep -o "/home/eray/informatics" <<< $PWD) != "" ]] && venv
ls
