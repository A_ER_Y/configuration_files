-- text.lua

vim.cmd([[syntax match TextComment /^\s*#.*$/]])
vim.cmd([[highlight link TextComment Comment]])
