-- plugins.lua

return require("packer").startup(function(use)
    use "wbthomason/packer.nvim"
    use {
        "nvim-treesitter/nvim-treesitter",
        { run = ":TSUpdate" }
    }
    use {
        "williamboman/mason.nvim",
        "RubixDev/mason-update-all",
        "williamboman/mason-lspconfig.nvim",
        "neovim/nvim-lspconfig"
    }
    use {
        "nvimdev/lspsaga.nvim",
        event = "LspAttach",
        opt = true
    }
    use {
        "hrsh7th/nvim-cmp",
        "hrsh7th/cmp-buffer",
        "hrsh7th/cmp-nvim-lsp",
        "hrsh7th/cmp-path"
    }
    use "lewis6991/gitsigns.nvim"
    use "norcalli/nvim-colorizer.lua"
end)
