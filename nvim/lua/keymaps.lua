-- keymaps.lua

vim.g.mapleader = ","
local options = { noremap = true, silent = true }

vim.keymap.set("", "j", "gj", options)
vim.keymap.set("", "k", "gk", options)
vim.keymap.set("", "J", "}", options)
vim.keymap.set("", "K", "{", options)
vim.keymap.set("", "H", ":normal g0hzH<CR>", options)
vim.keymap.set("", "L", ":normal g$lzL<CR>", options)
vim.keymap.set("v", "<c-j>", ":m '>+1<CR>gv=gv", options)
vim.keymap.set("v", "<c-k>", ":m '<-2<CR>gv=gv", options)
vim.keymap.set("", "<space>", "<c-f>", options)

vim.keymap.set("", "p", "]p", options)
vim.keymap.set("", "P", "]P", options)
vim.keymap.set("v", "p", [["_dP]], options)
vim.keymap.set("v", "P", [["_dp]], options)

vim.keymap.set("", "<leader>m", "ma", options)
vim.keymap.set("", "<c-m>", "'a", options)
vim.keymap.set("", "<s-m>", "''", options)

vim.keymap.set("n", "<c-j>", ":tabnext<CR>", options)
vim.keymap.set("n", "<c-k>", ":tabprevious<CR>", options)
vim.keymap.set("n", "<c-o>", ":tabonly<CR>", options)
vim.keymap.set("n", "<c-c>", ":tabclose<CR>", options)

vim.keymap.set("", "//", ":nohlsearch<bar>:echo<CR>", options)
vim.keymap.set("", "<leader>B", ":Gitsigns blame<CR>", options)
vim.keymap.set("", "<leader>b", ":Gitsigns toggle_current_line_blame<CR>", options)
vim.keymap.set("", "<leader>g", ":silent Gitsigns next_hunk<CR>", options)
vim.keymap.set("v", "<leader>n", "g<c-a>", options)
vim.keymap.set("", "<leader>w", ":set wrap!<CR>", options)

vim.keymap.set("", "<leader>e", function()
    vim.opt.cursorcolumn = not vim.opt.cursorcolumn:get()
    vim.opt.cursorline = not vim.opt.cursorline:get()
    vim.opt.number = not vim.opt.number:get()
    vim.cmd("silent! Gitsigns toggle_signs")
end, options)

vim.keymap.set("", "<leader>o", function()
    local word = vim.fn.expand("<cWORD>")
    vim.fn.execute(":!open " .. word)
end, options)
