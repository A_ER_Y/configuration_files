-- options.lua

vim.cmd("colorscheme aery")

vim.opt.belloff = "all"
vim.opt.mouse = ""
vim.opt.wrap = false
vim.opt.scrolloff = 80
vim.opt.clipboard = "unnamedplus"

vim.opt.cmdheight = 0
vim.opt.guicursor = ""
vim.opt.ruler = false
vim.opt.showcmd = false
vim.opt.showmode = false
vim.opt.laststatus = 0

vim.opt.hlsearch = true
vim.opt.ignorecase = true
vim.opt.incsearch = true
vim.opt.smartcase = true

vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 4
vim.opt.smartindent = true

local filetypes_with_tab_width_2 = { typescript = true, vue = true }
function set_tab_width_2_for_filetypes()
    local filetype = vim.bo.filetype
    if filetypes_with_tab_width_2[filetype] then
        vim.opt.shiftwidth = 2
        vim.opt.softtabstop = 2
        vim.opt.tabstop = 2
    end
end
vim.cmd("autocmd FileType * lua set_tab_width_2_for_filetypes()")
