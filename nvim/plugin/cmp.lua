-- cmp.lua

-- sources:
--  https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/
--  https://github.com/hrsh7th/nvim-cmp/blob/e28fb7a730b1bd425fdddfdbd3d36bb84bd77611/lua/cmp/config/compare.lua
--  https://github.com/lukas-reineke/cmp-under-comparator/blob/master/lua/cmp-under-comparator/init.lua
--  https://youtu.be/z5mqa4ELjhw?list=PLOe6AggsTaVuIXZU4gxWJpIQNHMrDknfN

local cmp = require("cmp")

local kind_mapper = require("cmp.types").lsp.CompletionItemKind
local kind_score = {
    Keyword     = 1,
    Property    = 2,
    Variable    = 3,
    Constant    = 4,
    Method      = 5,
    Function    = 6,
    Constructor = 7,
    Field       = 8,
    Class       = 9,
    Interface   = 10,
    Module      = 11
}

cmp.setup({
    mapping = {
        ["<S-Tab>"] = cmp.mapping.select_prev_item(),
        ["<Tab>"] = cmp.mapping.select_next_item(),
        ["<c-c>"] = cmp.mapping.complete()
    },
    sources = {
        { name = "nvim_lsp" },
        { name = "buffer" },
        { name = "path" }
    },
    sorting = {
        comparators = {
            cmp.config.compare.score,
            function(entry1, entry2)
                local _, underscore1 = entry1.completion_item.label:find "^_+"
                local _, underscore2 = entry2.completion_item.label:find "^_+"
                underscore1 = underscore1 or 0
                underscore2 = underscore2 or 0
                if underscore1 ~= underscore2 then
                    return underscore1 < underscore2
                end
            end,
            function(entry1, entry2)
                local kind1 = kind_score[kind_mapper[entry1:get_kind()]] or 100
                local kind2 = kind_score[kind_mapper[entry2:get_kind()]] or 100
                if kind1 ~= kind2 then
                    return kind1 < kind2
                end
            end,
            cmp.config.compare.sort_text,
        }
    },
    window = {
        completion = cmp.config.window.bordered({
            winhighlight = "Normal:Pmenu,FloatBorder:SagaBorder,Search:None"
        }),
        documentation = cmp.config.window.bordered({
            winhighlight = "Normal:Pmenu,FloatBorder:SagaBorder,Search:None"
        })
    },
    formatting = {
        format = function(entry, item)
            item.menu = ({
                nvim_lsp = "",
                buffer = "",
                path = ""
            })[entry.source.name]
            return item
        end
    }
})
