-- gitsigns.lua

require("gitsigns").setup({
    signs = {
        add             = { text = "+" },
        change          = { text = "~" },
        changedelete    = { text = "~_" },
        delete          = { text = "-" },
        topdelete       = { text = "-" },
        untracked       = { text = "" }
    },
    signs_staged = {
        add             = { text = "+" },
        change          = { text = "~" },
        changedelete    = { text = "~_" },
        delete          = { text = "-" },
        topdelete       = { text = "-" },
        untracked       = { text = "" }
    },
    signs_staged_enable = true,
    signcolumn = false,
    numhl = false,
    linehl = false,
    current_line_blame = false,
    current_line_blame_opts = {
        virt_text = true,
        virt_text_pos = "eol",
        delay = 0,
        ignore_whitespace = false,
        use_focus = false
    },
    current_line_blame_formatter = "<author>, <author_time:%Y-%m-%d> - <summary>"
})
