-- lsp.lua

local servers = { "graphql", "pyright", "ts_ls", "volar" }

require("mason").setup()
require("mason-update-all").setup()
require("mason-lspconfig").setup({
    ensure_installed = servers
})

local lspconfig = require("lspconfig")
local options = { noremap = true, silent = true }
local init_options = function(lsp)
    if lsp == "ts_ls" then
        return {
            plugins = {
                {
                    name = "@vue/typescript-plugin",
                    location = "/usr/lib/node_modules/@vue/typescript-plugin",
                    languages = { "javascript", "typescript", "vue" }
                }
            }
        }
    else
        return nill
    end
end
local filetypes = function(lsp)
    if lsp == "ts_ls" then
        return { "javascript", "typescript", "vue" }
    else
        return nill
    end
end

for _, lsp in ipairs(servers) do
    lspconfig[lsp].setup({
        init_options = init_options(lsp),
        filetypes = filetypes(lsp),
        on_attach = function()
            require("lspsaga").setup({
                ui = {
                    border = "rounded",
                    devicon = false,
                    title = true,
                    expand = "",
                    collapse = "",
                    code_action = "",
                    actionfix = "",
                    kind = {},
                    imp_sign = ""
                },
                symbol_in_winbar = {
                    enable = false
                },
                callhierarchy = {
                    layout = "float",
                    keys = {
                        tabe = "<CR>",
                        quit = { "q", "<ESC>" }
                    }
                },
                code_action = {
                    num_shortcut = true,
                    show_server_name = false,
                    extend_gitsigns = false,
                    keys = {
                        exec = "<CR>",
                        quit = { "q", "<ESC>" }
                    }
                },
                diagnostic = {
                    show_code_action = true,
                    jump_num_shortcut = true,
                    max_width = 1,
                    max_height = 0.7,
                    text_hl_follow = true,
                    border_follow = false,
                    extend_relatedInformation = true,
                    show_layout = "float",
                    show_normal_height = 1,
                    max_show_width = 1,
                    max_show_height = 0.7,
                    diagnostic_only_current = false
                },
                finder = {
                    max_height = 0.7,
                    left_width = 0.2,
                    rigth_width = 0.8,
                    default = "def+imp+ref",
                    methods = {},
                    layout = "float",
                    filter = {},
                    silent = true,
                    number = true,
                    keys = {
                        tabe = "<CR>",
                        quit = { "q", "<ESC>" }
                    }
                },
                hover = {
                    max_width = 0.7,
                    open_browser = "qutebrowser"
                },
                rename = {
                    in_select = false,
                    auto_save = false,
                    project_max_width = 0.5,
                    project_max_height = 0.5,
                    keys = {
                        exec = "<CR>",
                        quit = "<ESC>"
                    }
                }
            })

            vim.keymap.set("", "<leader>a", "<cmd>Lspsaga code_action<CR>", options)
            vim.keymap.set("", "<leader>c", "<cmd>Lspsaga incoming_calls<CR>", options)
            vim.keymap.set("", "<leader>d", "<cmd>Lspsaga diagnostic_jump_next<CR>", options)
            vim.keymap.set("", "<leader>f", "<cmd>Lspsaga finder<CR>", options)
            vim.keymap.set("", "<leader>i", "<cmd>Lspsaga hover_doc<CR>", options)
            vim.keymap.set("", "<leader>r", "<cmd>Lspsaga rename<CR>", options)
            vim.keymap.set("", "<leader>s", "<cmd>LspRestart<CR>", options)
        end
    })
end

vim.diagnostic.config({
    signs = true,
    virtual_text = false,
    underline = true,
    update_in_insert = false
})

local signs = { Error = "•", Warn = "•", Hint = "•", Info = "•" }
for type, icon in pairs(signs) do
    local highlight_group = "DiagnosticSign" .. type
    vim.fn.sign_define(highlight_group, {
        text = icon,
        texthl = highlight_group,
        numhl = highlight_group
    })
end
