-- aery.lua
-- :source $VIMRUNTIME/syntax/hitest.vim

-- reset syntax highlighting
vim.cmd("highlight clear")
vim.cmd("syntax reset")


-- settings
vim.g.colors_name = "aery"
vim.o.background = "dark"
vim.o.termguicolors = true
vim.cmd("syntax on")


-- font styles
local styles = {
    bold            = "bold",
    bold_italic     = "bold,italic",
    italic          = "italic",
    underline       = "underline",
    strikethrough   = "strikethrough",
    none            = "NONE"
}


-- colors
local colors = {
    background  = string.format("#%s", os.getenv("HEX_POPUP")),
    foreground  = string.format("#%s", os.getenv("HEX_WHITE")),
    highlight   = string.format("#%s", os.getenv("HEX_HIGHLIGHT")),

    blue        = string.format("#%s", os.getenv("HEX_BLUE")),
    cyan        = string.format("#%s", os.getenv("HEX_CYAN")),
    green       = string.format("#%s", os.getenv("HEX_GREEN")),
    grey        = string.format("#%s", os.getenv("HEX_GREY")),
    magenta     = string.format("#%s", os.getenv("HEX_MAGENTA")),
    orange      = string.format("#%s", os.getenv("HEX_ORANGE")),
    red         = string.format("#%s", os.getenv("HEX_RED")),
    white       = string.format("#%s", os.getenv("HEX_WHITE")),
    yellow      = string.format("#%s", os.getenv("HEX_YELLOW")),

    none        = "NONE"
}


-- syntax
local syntax = {
    comment             = colors.grey,

    constant            = colors.blue,
    string              = colors.orange,
    number              = colors.yellow,
    boolean             = colors.blue,

    variable            = colors.cyan,
    func                = colors.green,
    class               = colors.green,

    statement           = colors.magenta,
    conditional         = colors.magenta,
    rept                = colors.magenta,
    label               = colors.magenta,
    operator            = colors.white,
    keyword             = colors.magenta,
    exception           = colors.magenta,

    pre_proc            = colors.blue,
    include             = colors.magenta,
    define              = colors.blue,
    macro               = colors.blue,
    pre_condit          = colors.blue,

    type                = colors.blue,
    storage_class       = colors.magenta,
    structure           = colors.blue,
    typedef             = colors.blue,

    special             = colors.yellow,
    special_char        = colors.yellow,
    tag                 = colors.blue,
    delimiter           = colors.white,
    special_comment     = colors.grey,
    debug               = colors.yellow,

    info                = colors.cyan,
    hint                = colors.cyan,
    warning             = colors.yellow,
    error               = colors.red
}


-- highlight groups
local highlight_groups = {
    Comment         = { fg = syntax.comment,            bg = colors.none,           style = styles.italic },

    Constant        = { fg = syntax.constant,           bg = colors.none,           style = styles.none },
    String          = { fg = syntax.string,             bg = colors.none,           style = styles.none },
    Character       = { fg = syntax.string,             bg = colors.none,           style = styles.none },
    Number          = { fg = syntax.number,             bg = colors.none,           style = styles.none },
    Boolean         = { fg = syntax.boolean,            bg = colors.none,           style = styles.none },
    Float           = { fg = syntax.number,             bg = colors.none,           style = styles.none },

    Identifier      = { fg = syntax.variable,           bg = colors.none,           style = styles.none },
    Function        = { fg = syntax.func,               bg = colors.none,           style = styles.none },

    Statement       = { fg = syntax.statement,          bg = colors.none,           style = styles.none },
    Conditional     = { fg = syntax.conditional,        bg = colors.none,           style = styles.none },
    Repeat          = { fg = syntax.rept,               bg = colors.none,           style = styles.none },
    Label           = { fg = syntax.label,              bg = colors.none,           style = styles.none },
    Operator        = { fg = syntax.operator,           bg = colors.none,           style = styles.none },
    Keyword         = { fg = syntax.keyword,            bg = colors.none,           style = styles.none },
    Exception       = { fg = syntax.exception,          bg = colors.none,           style = styles.none },

    PreProc         = { fg = syntax.pre_proc,           bg = colors.none,           style = styles.none },
    Include         = { fg = syntax.include,            bg = colors.none,           style = styles.none },
    Define          = { fg = syntax.define,             bg = colors.none,           style = styles.none },
    Macro           = { fg = syntax.macro,              bg = colors.none,           style = styles.none },
    PreCondit       = { fg = syntax.pre_condit,         bg = colors.none,           style = styles.none },

    Type            = { fg = syntax.type,               bg = colors.none,           style = styles.none },
    StorageClass    = { fg = syntax.storage_class,      bg = colors.none,           style = styles.none },
    Structure       = { fg = syntax.structure,          bg = colors.none,           style = styles.none },
    Typedef         = { fg = syntax.typedef,            bg = colors.none,           style = styles.none },

    Special         = { fg = syntax.special,            bg = colors.none,           style = styles.none },
    SpecialChar     = { fg = syntax.special_char,       bg = colors.none,           style = styles.none },
    Delimiter       = { fg = syntax.delimiter,          bg = colors.none,           style = styles.none },
    SpecialComment  = { fg = syntax.special_comment,    bg = colors.none,           style = styles.bold_italic },
    Debug           = { fg = syntax.debug,              bg = colors.none,           style = styles.none },

    Underlined      = { fg = syntax.comment,            bg = colors.none,           style = styles.underline },
    Todo            = { fg = colors.foreground,         bg = colors.none,           style = styles.bold },
    Error           = { fg = syntax.error,              bg = colors.none,           style = styles.bold_italic },
    ErrorMsg        = { fg = syntax.error,              bg = colors.none,           style = styles.bold_italic },
    Question        = { fg = syntax.string,             bg = colors.none,           style = styles.none },
    WarningMsg      = { fg = syntax.warning,            bg = colors.none,           style = styles.italic },
    Search          = { fg = colors.none,               bg = colors.highlight,      style = styles.none },
    CurSearch       = { fg = colors.none,               bg = colors.highlight,      style = styles.none },

    Directory       = { fg = syntax.string,             bg = colors.none,           style = styles.none },
    CursorLine      = { fg = colors.none,               bg = colors.highlight,      style = styles.none },
    CursorColumn    = { fg = colors.none,               bg = colors.highlight,      style = styles.none },
    MatchParen      = { fg = syntax.operator,           bg = colors.highlight,      style = styles.none },
    ColorColumn     = { fg = colors.foreground,         bg = colors.background,     style = styles.none },

    Normal          = { fg = colors.foreground,         bg = colors.none,           style = styles.none },
    NormalFloat     = { fg = colors.none,               bg = colors.background,     style = styles.none },
    FloatTitle      = { fg = syntax.constant,           bg = colors.background,     style = styles.none },
    FloatBorder     = { fg = colors.highlight,          bg = colors.background,     style = styles.none },
    Visual          = { fg = colors.none,               bg = colors.highlight,      style = styles.none },
    Cursor          = { fg = colors.none,               bg = colors.none,           style = styles.none },
    iCursor         = { fg = colors.none,               bg = colors.none,           style = styles.none },
    LineNr          = { fg = syntax.comment,            bg = colors.none,           style = styles.italic },
    NonText         = { fg = syntax.comment,            bg = colors.none,           style = styles.none },
    CursorLineNr    = { fg = colors.foreground,         bg = colors.none,           style = styles.none },
    VertSplit       = { fg = colors.background,         bg = colors.none,           style = styles.none },
    Title           = { fg = syntax.constant,           bg = colors.background,     style = styles.none },
    Pmenu           = { fg = colors.foreground,         bg = colors.background,     style = styles.none },
    PmenuSel        = { fg = colors.none,               bg = colors.highlight,      style = styles.none },
    PmenuSbar       = { fg = colors.background,         bg = colors.background,     style = styles.none },
    PmenuThumb      = { fg = colors.background,         bg = colors.highlight,      style = styles.none },
    SignColumn      = { fg = colors.foreground,         bg = colors.none,           style = styles.bold },
    TabLine         = { fg = colors.foreground,         bg = colors.background,     style = styles.none },
    TabLineSel      = { fg = colors.foreground,         bg = colors.highlight,      style = styles.bold_italic },

    ["@variable"]               = { fg = syntax.variable,   bg = colors.none,   style = styles.none },
    ["@markup.strong"]          = { fg = colors.none,       bg = colors.none,   style = styles.bold },
    ["@markup.italic"]          = { fg = colors.none,       bg = colors.none,   style = styles.italic },
    ["@markup.strikethrough"]   = { fg = colors.none,       bg = colors.none,   style = styles.strikethrough },
    ["@markup.underline"]       = { fg = colors.none,       bg = colors.none,   style = styles.underline },

    DiagnosticError             = { fg = syntax.error,      bg = colors.none,   style = styles.none },
    DiagnosticSignError         = { fg = syntax.error,      bg = colors.none,   style = styles.bold },
    DiagnosticUnderlineError    = { fg = syntax.error,      bg = colors.none,   style = styles.bold_italic },
    DiagnosticWarn              = { fg = syntax.warning,    bg = colors.none,   style = styles.none },
    DiagnosticSignWarn          = { fg = syntax.warning,    bg = colors.none,   style = styles.bold },
    DiagnosticUnderlineWarn     = { fg = syntax.warning,    bg = colors.none,   style = styles.bold_italic },
    DiagnosticHint              = { fg = syntax.hint,       bg = colors.none,   style = styles.none },
    DiagnosticSignHint          = { fg = syntax.hint,       bg = colors.none,   style = styles.bold },
    DiagnosticUnderlineHint     = { fg = syntax.comment,    bg = colors.none,   style = styles.italic },
    DiagnosticInfo              = { fg = syntax.info,       bg = colors.none,   style = styles.none },
    DiagnosticSignInfo          = { fg = syntax.info,       bg = colors.none,   style = styles.bold },
    DiagnosticUnderlineInfo     = { fg = syntax.info,       bg = colors.none,   style = styles.bold_italic },
    TrailingWhitespace          = { fg = colors.none,       bg = "red",         style = styles.none },

    SagaNormal          = { fg = colors.none,           bg = colors.background,     style = styles.none },
    SagaBorder          = { fg = colors.highlight,      bg = colors.background,     style = styles.none },
    CodeActionNumber    = { fg = colors.none,           bg = colors.background,     style = styles.none },
    RenameNormal        = { fg = colors.none,           bg = colors.background,     style = styles.none },

    CmpItemAbbrMatch        = { fg = colors.red,            bg = colors.none,   style = styles.none },
    CmpItemKindClass        = { fg = syntax.keyword,        bg = colors.none,   style = styles.italic },
    CmpItemKindField        = { fg = syntax.keyword,        bg = colors.none,   style = styles.italic },
    CmpItemKindKeyword      = { fg = syntax.keyword,        bg = colors.none,   style = styles.italic },
    CmpItemKindModule       = { fg = colors.blue,           bg = colors.none,   style = styles.italic },
    CmpItemKindFunction     = { fg = syntax.func,           bg = colors.none,   style = styles.italic },
    CmpItemKindMethod       = { fg = syntax.func,           bg = colors.none,   style = styles.italic },
    CmpItemKindVariable     = { fg = syntax.variable,       bg = colors.none,   style = styles.italic },
    CmpItemKindText         = { fg = colors.foreground,     bg = colors.none,   style = styles.italic },

    GitSignsAdd                 = { fg = colors.green,      bg = colors.none,   style = styles.bold },
    GitSignsChange              = { fg = colors.yellow,     bg = colors.none,   style = styles.bold },
    GitSignsChangedelete        = { fg = colors.orange,     bg = colors.none,   style = styles.bold },
    GitSignsDelete              = { fg = colors.red,        bg = colors.none,   style = styles.bold },
    GitSignsTopdelete           = { fg = colors.red,        bg = colors.none,   style = styles.bold },
    GitSignsUntracked           = { fg = colors.none,       bg = colors.none,   style = styles.none },
    GitSignsCurrentLineBlame    = { fg = syntax.comment,    bg = colors.none,   style = styles.italic }
}


-- highlight
vim.cmd("match TrailingWhitespace /\\s\\+$/")

for group, highlight in pairs(highlight_groups) do
    vim.api.nvim_command(
        "highlight " .. group ..
        " guifg=" .. highlight.fg ..
        " guibg=" .. highlight.bg ..
        " gui=" .. highlight.style ..
        " cterm=" .. highlight.style
    )
end
