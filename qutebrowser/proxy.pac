function FindProxyForURL(url, host) {
    if (shExpMatch(host, "*.inuits.consul"))
        return "SOCKS5 localhost:10001";
    if (shExpMatch(host, "*.keyprod.inuits.eu"))
        return "SOCKS5 localhost:10001";

    if (shExpMatch(host, "*.coghent.consul"))
        return "SOCKS5 localhost:11001";

    if (shExpMatch(host, "*.poa-anpr.antwerpen.be"))
        return "SOCKS5 localhost:11002";
    if (shExpMatch(host, "*.poa-prod.consul"))
        return "SOCKS5 localhost:11002";

    if (shExpMatch(host, "*.politie.antwerpen.be"))
        return "SOCKS5 localhost:11003";
    if (shExpMatch(host, "*.pzanomad.consul"))
        return "SOCKS5 localhost:11003";

    return "DIRECT";
}
