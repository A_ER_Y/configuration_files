# config.py
# pyright: reportUndefinedVariable=false

from os import getenv


def set_auto_save_settings():
    config.set("auto_save.interval", 15000)
    config.set("auto_save.session", False)


def bind_settings():
    config.bind("b", "config-cycle statusbar.show never always;; config-cycle tabs.show switching always")
    config.bind(",r", "config-source /home/eray/informatics/os/linux/configuration_files/qutebrowser/config.py;; reload")
    config.bind(",i", "set -u {url:domain} content.images True;; reload")
    config.bind(",s", "set -u {url:domain} content.javascript.enabled True;; reload")
    config.bind(",c", "set -u {url:domain} content.images True;; set -u {url:domain} content.cookies.accept all;; set -u {url:domain} content.javascript.enabled True;; set -u {url:domain} content.local_storage True;; reload")
    config.bind(",C", "set -t content.images True;; set -t content.cookies.accept all;; set -t content.javascript.enabled True;; set -t content.local_storage True;; reload")
    config.bind("I", "mode-enter passthrough")


def set_color_settings():
    black       = f"#{getenv('HEX_BLACK')}"
    cyan        = f"#{getenv('HEX_CYAN')}"
    green       = f"#{getenv('HEX_GREEN')}"
    grey        = f"#{getenv('HEX_GREY')}"
    orange      = f"#{getenv('HEX_ORANGE')}"
    red         = f"#{getenv('HEX_RED')}"
    white       = f"#{getenv('HEX_WHITE')}"
    yellow      = f"#{getenv('HEX_YELLOW')}"
    highlight   = f"#{getenv('HEX_HIGHLIGHT')}"

    config.set("colors.completion.category.bg", black)
    config.set("colors.completion.category.border.bottom", black)
    config.set("colors.completion.category.border.top", black)
    config.set("colors.completion.category.fg", grey)
    config.set("colors.completion.fg", [white, white, white])
    config.set("colors.completion.even.bg", black)
    config.set("colors.completion.odd.bg", black)
    config.set("colors.completion.item.selected.bg", highlight)
    config.set("colors.completion.item.selected.border.bottom", black)
    config.set("colors.completion.item.selected.border.top", black)
    config.set("colors.completion.item.selected.fg", white)
    config.set("colors.completion.item.selected.match.fg", red)
    config.set("colors.completion.match.fg", red)
    config.set("colors.completion.scrollbar.bg", black)
    config.set("colors.completion.scrollbar.fg", white)

    config.set("colors.contextmenu.disabled.bg", black)
    config.set("colors.contextmenu.disabled.fg", grey)
    config.set("colors.contextmenu.menu.bg", black)
    config.set("colors.contextmenu.menu.fg", white)
    config.set("colors.contextmenu.selected.bg", highlight)
    config.set("colors.contextmenu.selected.fg", white)

    config.set("colors.downloads.bar.bg", black)
    config.set("colors.downloads.error.bg", red)
    config.set("colors.downloads.error.fg", white)
    config.set("colors.downloads.start.bg", yellow)
    config.set("colors.downloads.start.fg", black)
    config.set("colors.downloads.stop.bg", green)
    config.set("colors.downloads.stop.fg", black)
    config.set("colors.downloads.system.bg", "rgb")
    config.set("colors.downloads.system.fg", "rgb")

    config.set("colors.hints.bg", yellow)
    config.set("colors.hints.fg", black)
    config.set("colors.hints.match.fg", green)
    config.set("colors.keyhint.bg", black)
    config.set("colors.keyhint.fg", white)
    config.set("colors.keyhint.suffix.fg", yellow)
    config.set("colors.tooltip.bg", black)
    config.set("colors.tooltip.fg", white)

    config.set("colors.messages.error.bg", red)
    config.set("colors.messages.error.border", red)
    config.set("colors.messages.error.fg", white)
    config.set("colors.messages.info.bg", black)
    config.set("colors.messages.info.border", black)
    config.set("colors.messages.info.fg", white)
    config.set("colors.messages.warning.bg", orange)
    config.set("colors.messages.warning.border", orange)
    config.set("colors.messages.warning.fg", white)

    config.set("colors.prompts.bg", black)
    config.set("colors.prompts.border", f"1px solid {grey}")
    config.set("colors.prompts.fg", white)
    config.set("colors.prompts.selected.bg", grey)
    config.set("colors.prompts.selected.fg", white)

    config.set("colors.statusbar.caret.bg", black)
    config.set("colors.statusbar.caret.fg", white)
    config.set("colors.statusbar.caret.selection.bg", highlight)
    config.set("colors.statusbar.caret.selection.fg", white)
    config.set("colors.statusbar.command.bg", black)
    config.set("colors.statusbar.command.fg", white)
    config.set("colors.statusbar.command.private.bg", black)
    config.set("colors.statusbar.command.private.fg", white)
    config.set("colors.statusbar.insert.bg", black)
    config.set("colors.statusbar.insert.fg", white)
    config.set("colors.statusbar.normal.bg", black)
    config.set("colors.statusbar.normal.fg", white)
    config.set("colors.statusbar.passthrough.bg", black)
    config.set("colors.statusbar.passthrough.fg", white)
    config.set("colors.statusbar.private.bg", black)
    config.set("colors.statusbar.private.fg", white)
    config.set("colors.statusbar.progress.bg", cyan)
    config.set("colors.statusbar.url.error.fg", red)
    config.set("colors.statusbar.url.fg", white)
    config.set("colors.statusbar.url.hover.fg", cyan)
    config.set("colors.statusbar.url.success.http.fg", yellow)
    config.set("colors.statusbar.url.success.https.fg", green)
    config.set("colors.statusbar.url.warn.fg", orange)

    config.set("colors.tabs.bar.bg", black)
    config.set("colors.tabs.even.bg", black)
    config.set("colors.tabs.even.fg", grey)
    config.set("colors.tabs.odd.bg", black)
    config.set("colors.tabs.odd.fg", grey)
    config.set("colors.tabs.selected.even.bg", black)
    config.set("colors.tabs.selected.even.fg", white)
    config.set("colors.tabs.selected.odd.bg", black)
    config.set("colors.tabs.selected.odd.fg", white)
    config.set("colors.tabs.pinned.even.bg", black)
    config.set("colors.tabs.pinned.even.fg", grey)
    config.set("colors.tabs.pinned.odd.bg", black)
    config.set("colors.tabs.pinned.odd.fg", grey)
    config.set("colors.tabs.pinned.selected.even.bg", black)
    config.set("colors.tabs.pinned.selected.even.fg", white)
    config.set("colors.tabs.pinned.selected.odd.bg", black)
    config.set("colors.tabs.pinned.selected.odd.fg", white)
    config.set("colors.tabs.indicator.error", red)
    config.set("colors.tabs.indicator.start", yellow)
    config.set("colors.tabs.indicator.stop", green)
    config.set("colors.tabs.indicator.system", "rgb")

    config.set("colors.webpage.bg", "#ffffff")
    config.set("colors.webpage.darkmode.algorithm", "lightness-cielab")
    config.set("colors.webpage.darkmode.contrast", 0.0)
    config.set("colors.webpage.darkmode.enabled", False)
    config.set("colors.webpage.darkmode.policy.images", "never")
    config.set("colors.webpage.darkmode.policy.page", "always")
    config.set("colors.webpage.darkmode.threshold.background", 0)
    config.set("colors.webpage.darkmode.threshold.foreground", 256)
    config.set("colors.webpage.preferred_color_scheme", "dark")


def set_completion_settings():
    config.set("completion.cmd_history_max_items", 100)
    config.set("completion.delay", 0)
    config.set("completion.favorite_paths", [])
    config.set("completion.height", "35%")
    config.set("completion.min_chars", 1)
    config.set("completion.open_categories", [
        "history",
        "quickmarks",
        "bookmarks"
    ])
    config.set("completion.quick", True)
    config.set("completion.scrollbar.padding", 0)
    config.set("completion.scrollbar.width", 0)
    config.set("completion.show", "always")
    config.set("completion.shrink", False)
    config.set("completion.timestamp_format", "%Y-%m-%d %H:%M")
    config.set("completion.use_best_match", True)
    config.set("completion.web_history.exclude", [])
    config.set("completion.web_history.max_items", -1)


def set_content_settings():
    config.set("content.blocking.adblock.lists", [
        "https://easylist.to/easylist/easylist.txt",
        "https://easylist.to/easylist/easyprivacy.txt",
        "https://easylist-downloads.adblockplus.org/easylistdutch+easylist.txt",
        "https://easylist-downloads.adblockplus.org/easylistgermany+easylist.txt",
        "https://easylist-downloads.adblockplus.org/liste_fr+easylist.txt",
        "https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt",
        "https://raw.githubusercontent.com/Spam404/lists/master/adblock-list.txt",
        "https://easylist-downloads.adblockplus.org/fanboy-notifications.txt",
        "https://raw.githubusercontent.com/bongochong/CombinedPrivacyBlockLists/master/cpbl-abp-list.txt",
        "https://easylist-downloads.adblockplus.org/fanboy-social.txt"
    ])
    config.set("content.blocking.enabled", True)
    config.set("content.blocking.hosts.block_subdomains", True)
    config.set("content.blocking.hosts.lists", [])
    config.set("content.blocking.method", "adblock")
    config.set("content.blocking.whitelist", [])

    config.set("content.cache.appcache", True)
    config.set("content.cache.maximum_pages", 0)
    config.set("content.cache.size", None)
    config.set("content.cookies.accept", "never")
    config.set("content.cookies.store", True)

    config.set("content.canvas_reading", True)
    config.set("content.default_encoding", "iso-8859-1")
    config.set("content.dns_prefetch", True)
    config.set("content.frame_flattening", False)
    config.set("content.fullscreen.overlay_timeout", 3000)
    config.set("content.fullscreen.window", True)
    config.set("content.geolocation", False)

    config.set("content.headers.accept_language", "en-US,en;q=0.9")
    config.set("content.headers.custom", {})
    config.set("content.headers.do_not_track", True)
    config.set("content.headers.referer", "same-domain")
    config.set("content.headers.user_agent", "Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}", "https://web.whatsapp.com/")
    config.set("content.headers.user_agent", "Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version} Edg/{upstream_browser_version}", "https://accounts.google.com/*")
    config.set("content.headers.user_agent", "Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36", "https://*.slack.com/*")

    config.set("content.javascript.alert", False)
    config.set("content.javascript.can_close_tabs", False)
    config.set("content.javascript.can_open_tabs_automatically", False)
    config.set("content.javascript.clipboard", "access")
    config.set("content.javascript.enabled", False)
    config.set("content.javascript.legacy_touch_events", "never")
    config.set("content.javascript.log", {
        "error": "debug",
        "info": "debug",
        "unknown": "debug",
        "warning": "debug"
    })
    config.set("content.javascript.modal_dialog", False)
    config.set("content.javascript.prompt", False)

    config.set("content.local_content_can_access_file_urls", True)
    config.set("content.local_content_can_access_remote_urls", False)
    config.set("content.local_storage", False)
    config.set("content.persistent_storage", False)

    config.set("content.autoplay", False)
    config.set("content.images", False)
    config.set("content.desktop_capture", False)
    config.set("content.media.audio_capture", False)
    config.set("content.media.audio_video_capture", False)
    config.set("content.media.video_capture", False)

    config.set("content.mouse_lock", False)
    config.set("content.mute", False)
    config.set("content.netrc_file", "")
    config.set("content.notifications.enabled", False)
    config.set("content.notifications.presenter", "auto")
    config.set("content.notifications.show_origin", True)
    config.set("content.pdfjs", False)
    config.set("content.plugins", False)
    config.set("content.prefers_reduced_motion", True)
    config.set("content.print_element_backgrounds", True)
    config.set("content.user_stylesheets", [])

    config.set("content.hyperlink_auditing", False)
    config.set("content.private_browsing", False)
    config.set("content.proxy", "system")
    config.set("content.proxy_dns_requests", True)
    config.set("content.register_protocol_handler", False)
    config.set("content.site_specific_quirks.enabled", True)
    config.set("content.site_specific_quirks.skip", ["js-string-replaceall"])
    config.set("content.tls.certificate_errors", "ask")
    config.set("content.unknown_url_scheme_policy", "allow-from-user-interaction")
    config.set("content.webgl", True)
    config.set("content.webrtc_ip_handling_policy", "all-interfaces")
    config.set("content.xss_auditing", False)

    exceptional_urls = [
        "chrome-devtools://*",
        "chrome://*/*",
        "devtools://*",
        "file:///*",
        "http://*.consul/*",
        "http://*.localhost/*",
        "http://127.0.0.1:*/*",
        "http://localhost:*/*",
        "https://*.google.com/*",
        "https://*.inuits.dev/*",
        "https://*.inuits.eu/*",
        "https://*.inuits.io/*",
        "https://*.inuits.se/*",
        "https://*.inuits.uat/*",
        "https://*.jit.si/*",
        "https://*.live.com/*",
        "https://*.microsoft.com/*",
        "https://*.microsoftonline.com/*",
        "https://*.office.com",
        "https://*.office365.com/*",
        "https://*.openai.com/*",
        "https://*.poa-anpr.antwerpen.be/*",
        "https://*.politie.antwerpen.be/*",
        "https://*.sharepoint.com/*",
        "https://*.visualstudio.com/*",
        "https://127.0.0.1:*/*",
        "https://chatgpt.com/*",
        "https://github.com/*",
        "https://gitlab.com/*",
        "https://localhost:*/*",
        "https://safe.search.brave.com/*",
        "qute://*/*"
    ]
    for url in exceptional_urls:
        config.set("content.cookies.accept", "all", url)
        config.set("content.javascript.can_close_tabs", True, url)
        config.set("content.javascript.can_open_tabs_automatically", True, url)
        config.set("content.javascript.enabled", True, url)
        config.set("content.local_storage", True, url)

        config.set("content.autoplay", True, url)
        if url != "https://safe.search.brave.com/*":
            config.set("content.images", True, url)
        config.set("content.desktop_capture", True, url)
        config.set("content.media.audio_capture", True, url)
        config.set("content.media.audio_video_capture", True, url)
        config.set("content.media.video_capture", True, url)


def set_download_settings():
    config.set("downloads.location.directory", "/home/eray/temporary")
    config.set("downloads.location.prompt", True)
    config.set("downloads.location.remember", False)
    config.set("downloads.location.suggestion", "filename")
    config.set("downloads.open_dispatcher", "")
    config.set("downloads.position", "bottom")
    config.set("downloads.prevent_mixed_content", True)
    config.set("downloads.remove_finished", 0)


def set_editor_settings():
    config.set("editor.encoding", "utf-8")
    config.set("editor.remove_file", True)


def set_hints_settings():
    config.set("hints.auto_follow", "unique-match")
    config.set("hints.auto_follow_timeout", 0)
    config.set("hints.border", "0")
    config.set("hints.chars", "qsdfghjklm")
    config.set("hints.dictionary", "/usr/share/dict/words")
    config.set("hints.find_implementation", "python")
    config.set("hints.hide_unmatched_rapid_hints", True)
    config.set("hints.leave_on_load", False)
    config.set("hints.min_chars", 1)
    config.set("hints.mode", "letter")
    config.set("hints.padding", { "bottom": 0, "left": 3, "right": 3, "top": 0 })
    config.set("hints.radius", 3)
    config.set("hints.scatter", True)
    config.set("hints.uppercase", False)


def set_input_settings():
    config.set("input.escape_quits_reporter", True)
    config.set("input.forward_unbound_keys", "auto")
    config.set("input.insert_mode.auto_enter", True)
    config.set("input.insert_mode.auto_leave", True)
    config.set("input.insert_mode.auto_load", False)
    config.set("input.insert_mode.leave_on_load", False)
    config.set("input.insert_mode.plugins", False)
    config.set("input.links_included_in_focus_chain", True)
    config.set("input.match_counts", True)
    config.set("input.media_keys", False)
    config.set("input.mode_override", "")
    config.set("input.mouse.back_forward_buttons", True)
    config.set("input.mouse.rocker_gestures", False)
    config.set("input.partial_timeout", 0)
    config.set("input.spatial_navigation", False)


def set_keyhint_settings():
    config.set("keyhint.blacklist", [])
    config.set("keyhint.delay", 500)
    config.set("keyhint.radius", 3)


def set_logging_settings():
    config.set("logging.level.console", "info")
    config.set("logging.level.ram", "debug")


def set_new_instance_settings():
    config.set("new_instance_open_target", "window")
    config.set("new_instance_open_target_window", "last-focused")


def set_prompt_settings():
    config.set("prompt.filebrowser", False)
    config.set("prompt.radius", 3)


def set_qt_settings():
    config.set("qt.args", getenv("CHROMIUM_FLAGS", "").replace("--", "").split(" "))
    config.set("qt.chromium.experimental_web_platform_features", "auto")
    config.set("qt.chromium.low_end_device_mode", "never")
    config.set("qt.chromium.process_model", "process-per-site-instance")
    config.set("qt.chromium.sandboxing", "enable-all")
    config.set("qt.environ", {})
    config.set("qt.force_platform", "")
    config.set("qt.force_platformtheme", "")
    config.set("qt.force_software_rendering", "none")
    config.set("qt.highdpi", False)
    config.set("qt.workarounds.disable_accelerated_2d_canvas", "never")
    config.set("qt.workarounds.disable_hangouts_extension", False)
    config.set("qt.workarounds.locale", False)
    config.set("qt.workarounds.remove_service_workers", False)


def set_scrolling_settings():
    config.set("scrolling.bar", "never")
    config.set("scrolling.smooth", False)


def set_search_settings():
    config.set("search.ignore_case", "smart")
    config.set("search.incremental", True)
    config.set("search.wrap", True)
    config.set("search.wrap_messages", True)


def set_session_settings():
    config.set("session.default_name", "")
    config.set("session.lazy_restore", False)


def set_statusbar_settings():
    config.set("statusbar.padding", {"bottom": 1, "left": 0, "right": 0, "top": 1})
    config.set("statusbar.position", "bottom")
    config.set("statusbar.show", "never")
    config.set("statusbar.widgets", ["keypress", "url", "scroll", "history", "tabs", "progress"])


def set_tabs_settings():
    config.set("tabs.background", True)
    config.set("tabs.close_mouse_button", "right")
    config.set("tabs.close_mouse_button_on_bar", "new-tab")
    config.set("tabs.favicons.scale", 1.0)
    config.set("tabs.favicons.show", "always")
    config.set("tabs.focus_stack_size", 10)
    config.set("tabs.indicator.padding", {"bottom": 2, "left": 0, "right": 4, "top": 2})
    config.set("tabs.indicator.width", 3)
    config.set("tabs.last_close", "ignore")
    config.set("tabs.max_width", -1)
    config.set("tabs.min_width", -1)
    config.set("tabs.mode_on_change", "normal")
    config.set("tabs.mousewheel_switching", True)
    config.set("tabs.new_position.related", "next")
    config.set("tabs.new_position.stacking", True)
    config.set("tabs.new_position.unrelated", "last")
    config.set("tabs.padding", {"bottom": 0, "left": 5, "right": 5, "top": 0})
    config.set("tabs.pinned.frozen", True)
    config.set("tabs.pinned.shrink", True)
    config.set("tabs.position", "top")
    config.set("tabs.select_on_remove", "next")
    config.set("tabs.show", "switching")
    config.set("tabs.show_switching_delay", 1500)
    config.set("tabs.tabs_are_windows", False)
    config.set("tabs.title.alignment", "left")
    config.set("tabs.title.elide", "right")
    config.set("tabs.title.format", "{audio}{index}: {current_title}")
    config.set("tabs.title.format_pinned", "{index}")
    config.set("tabs.tooltips", True)
    config.set("tabs.undo_stack_size", 100)
    config.set("tabs.width", "15%")
    config.set("tabs.wrap", True)


def set_url_settings():
    config.set("url.auto_search", "naive")
    config.set("url.default_page", "about:blank")
    config.set("url.incdec_segments", ["path", "query"])
    config.set("url.open_base_url", False)
    config.set("url.searchengines", {"DEFAULT": "https://safe.search.brave.com/search?q={}"})
    config.set("url.start_pages", "https://chatgpt.com/")
    config.set("url.yank_ignored_parameters", [
        "ref",
        "utm_source",
        "utm_medium",
        "utm_campaign",
        "utm_term",
        "utm_content"
    ])


def set_window_settings():
    config.set("window.hide_decoration", True)
    config.set("window.title_format", "{perc}{current_title}{title_sep}qutebrowser")
    config.set("window.transparent", False)


def set_zoom_settings():
    config.set("zoom.default", "110%")
    config.set("zoom.mouse_divider", 512)
    config.set("zoom.text_only", False)


def main():
    config.load_autoconfig(True)
    config.set("aliases", {"q": "close"})
    config.set("backend", "webengine")
    config.set("changelog_after_upgrade", "patch")
    config.set("confirm_quit", ["multiple-tabs", "downloads"])
    config.set("history_gap_interval", 30)
    config.set("messages.timeout", 3000)
    config.set("spellcheck.languages", [])

    set_auto_save_settings()
    bind_settings()
    set_color_settings()
    set_completion_settings()
    set_content_settings()
    set_download_settings()
    set_editor_settings()
    set_hints_settings()
    set_input_settings()
    set_keyhint_settings()
    set_logging_settings()
    set_new_instance_settings()
    set_prompt_settings()
    set_qt_settings()
    set_scrolling_settings()
    set_search_settings()
    set_session_settings()
    set_statusbar_settings()
    set_tabs_settings()
    set_url_settings()
    set_window_settings()
    set_zoom_settings();


main()
